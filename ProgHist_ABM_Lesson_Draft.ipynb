{
  "nbformat": 4,
  "nbformat_minor": 0,
  "metadata": {
    "colab": {
      "provenance": [],
      "toc_visible": true
    },
    "kernelspec": {
      "name": "python3",
      "display_name": "Python 3"
    },
    "language_info": {
      "name": "python"
    }
  },
  "cells": [
    {
      "cell_type": "markdown",
      "source": [
        "# “Modeling and Simulating Letter Exchange with Agent-based Modeling and the ‘mesa’  package”\n",
        "\n",
        "### Overview\n",
        "In this lesson, we want to give you an introduction to the simulation method of Agent-based Modeling (ABM) via the example of a historical model of letter exchange, implemented with the python-package 'mesa'.\n",
        "\n",
        "Users of different skill levels may find this lesson useful, for example if:\n",
        "- you are completely unfamiliar with ABM and want to get an overview what it is about conceptually\n",
        "- your already know about ABM conceptually and are wondering wether it can be useful for your own research project\n",
        "- you already know that ABM might be useful for your research and now want to learn about how the process of modeling and technical implementation of an ABM can work\n",
        "- you are familiar with all of the above, but need a starting point for implementing ABMs with 'mesa'\n",
        "\n",
        "If you are a complete newcomer to simulation methods whatsoever, we recommend to read up a little before proceeding. We made another tutorial during our project which introducing you to simulations starting from the very ground up!\n",
        "\n",
        "In general though, we will try to make sure to provide our conceptual and technical explanations with comments on the bigger picture, i.e., how ABM methodology relates to different kinds of historical research, how to think about ABMs as a historian, and talk about how to possibly deal with some of the existing desiderata when it comes to ABM for historical research.\n",
        "\n",
        "As a case study, we will use openly available letter data from the period of the 'Republic of Letters', sourced from the [LetterSampo](https://lettersampo.demo.seco.cs.aalto.fi/en)-project (more on that below) and will try to model the dynamics behind the letter sending that lead to the correspondance networks observed in that data-set. The goal of this (very basic) model is to better understand the social dynamics of correspondence networks and their impact on the exchange of ideas. This model won't be sufficiently complex to give answers to this research question, but it will highlight some key properties of ABM and ways to implement them.\n",
        "\n",
        "This hopefully serves as a useful example for all of you that also deal with any kind of correspondence or communication sources, or with the distribution and evolution of any kind of knowledge. We will also mention alternative approaches to the data as well as ABM applications for other research areas and data types. In any case, the technical basics we teach about 'mesa' will be of use to you, even if you don't work with letters!\n",
        "\n",
        "---\n",
        "---\n",
        "\n",
        "### Lesson Goals Summarized\n",
        "This lesson intends to:\n",
        "- teach conceptual basics of 'Agent-based Modeling and Simulation' for historians,\n",
        "- teach fundamentals of the python-package 'mesa' for programming ABMs,\n",
        "- give you guidance and resources for extending your ABM knowledge beyond this tutorial, as well as\n",
        "- give an overview over caveats, challenges, and things to think about when programming your own historical ABMs.\n",
        "\n",
        "\n",
        "---\n",
        "---\n"
      ],
      "metadata": {
        "id": "x8VmOe--6FM3"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### Technical Requirements\n",
        "\n",
        "For this lesson, only mesa and its dependencies are necessary. Of course, for your own research, you can use other libraries such as pandas or numpy to transform your data. Mesa should be compatible with other python libraries in almost all cases.\n",
        "\n",
        "Execute the following code block to install mesa and its dependencies. If you want to follow through the tutorial on your local machine, you need to set up an environment with mesa installed. If you don't know how to do this, we have a simple [step-by-step instruction](https://gitlab.gwdg.de/modelsen/abm-workshop-setup-instructions), which we compiled for a workshop.\n",
        "\n",
        "<details>\n",
        "<summary>Setup an environment</summary>\n",
        "If you already have Python installed, running the following code in a terminal should give you a new virtual environment with the mesa package:\n",
        "\n",
        "```\n",
        "python3 -m venv env\n",
        "source env/bin/activate\n",
        "pip install mesa\n",
        "```\n",
        "If you would like to have a more gentle introduction, head over to the tutorial introducing [Python](https://programminghistorian.org/en/lessons/introduction-and-installation).\n",
        "</details>"
      ],
      "metadata": {
        "id": "nOIqa30O5XUp"
      }
    },
    {
      "cell_type": "code",
      "execution_count": null,
      "metadata": {
        "id": "7M8Yfoi1f3Bk"
      },
      "outputs": [],
      "source": [
        "try:\n",
        "  import mesa\n",
        "except:\n",
        "  !pip install mesa"
      ]
    },
    {
      "cell_type": "markdown",
      "source": [
        "However, before we dive into the technical side of ABM, in the next parts we want to give you some background on what ABM is about, what it has to offer for historical research, and how it might relate to your research!\n"
      ],
      "metadata": {
        "id": "uoyqtr2fBH0L"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "## Part 1: Introduction to Agent-based Modeling\n",
        "\n",
        "### 1.1: What is Agent-based Modeling?\n",
        "`This part as well as the following (\"Why ABM for History) are copies (for now) of a jupyter notebook on the topic of ABM, which can be found on our project website: https://modelsen.mpiwg-berlin.mpg.de/jupyterbooks/book/abmintro/`\n",
        "\n",
        "`For the ProgHist tutorial, we would revise and extend these. The citations also haven't been converted yet to be displayed properly here.`\n",
        "\n",
        "Agent-based modeling (ABM for short) is a simulation method where interactions between agents of a system and the interplay of these interactions with an environment are modeled and simulated. These interactions should ideally produce emergent patterns that may correspond to real-world phenomena. Agents of the real system need to be explicitly and individually represented in the model.\n",
        "The goal of this method is to link the emergent patterns and phenomena at the systemic macro-level with the individual micro-level behavior of the agents.\n",
        "\n",
        "Agent-based modeling is a form of individual-based (sometimes also called micro-level) approach that differs from equation-based (or macro-level) approaches, in which only macro processes and at most aggregated groups of actors are modeled, not single agents or individuals {cite}davidsson_types_2017. Agent-based modeling differs from other micro-level approaches as well, in that each agent is \"explicitly and individually represented in the model\" and the intensity and variability of the interaction between the agents is particularly high. Apart from this explicit, individual representation, \"no further assumptions are made.\" {cite}galan_checking_2017.\n",
        "\n",
        "While mathematics can still play a major role in it, ABM is not as dependent on equations as other approaches and hence is more flexible. Purely mathematical approaches, while very effective for certain applications, are limiting in a number of ways that are especially relevant for history. Some of the aspects that are much harder to model mathematically rather than with ABM are agent heterogeneity, interdependencies between processes, out-of-equilibrium dynamics, local interactions, the role of physical space and finally the micro-macro link, so how macro-level phenomena are interconnected to individual behavior {cite}epstein_growing_1996.\n"
      ],
      "metadata": {
        "id": "88Un4TlFHqcX"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "<video width=120 height=100 src=\"mesa_running.webm\" controls>\n",
        "A visualization of a simulation run with Mesa.\n",
        "</video>"
      ],
      "metadata": {
        "id": "8-66bUL2Bed9"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### 1.2: Brief Historical Context of ABM\n",
        "\n",
        "`THIS IS PLACEHOLDER TEXT`\n",
        "\n",
        "`Both the content as well as the structure are WIP.`\n",
        "\n",
        "ABM was conceived as a separate simulation method during the 1990s, pioneered among others by Robert Axtell... . Similar, individual-based simulation approaches have existed for at least the 1960s, though. Tim Gooding puts the origins of ABM at 1933, when Enrico Fermi first used the so-called Monte-Carlo-Method with mechanical computing machines (Agent-Based Model History and Development, 2019).\n",
        "\n",
        "A number of distinguishing factors and changes since then: increase in power of hardware + affordability of higher perfomances, object-oriented programming languages / PLs specifically designed for ABM (such as NetLogo), complex adaptive systems thought / in particular phenomenons of emergence and adaptation/learning, coupling of macro and micro levels, emphasis on heterogeneity of actors, emphasis on theory-agnosticism. Historical ABMs, ie., ABMs that try to model historical processes, were part of the method community from the start, although often without the participation of actual historians. Mostly sociologists, economists, or other social and natural scientists that worked also with historial data. Exception are Archaeologists, which adopted the method also quite early and developed an active community. In historical science itself, until now only sporadic and often exploratory application, although a noticeable uptick of ABM research and general interest in simulation methods for historians in recent years. Discussions around the prerequisites of applying ABM for historical research are still ongoing, and there is also some fundamental pushback by some scholars, but .... the discussion is becoming more and more constructive, concentrating the scholarly effort on how to make the method increasingly useful for historians .....\n"
      ],
      "metadata": {
        "id": "k0PP5ZztEzEM"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### 1.3: What is ABM used for?\n",
        "\n",
        "`THIS IS PLACEHOLDER TEXT`\n",
        "\n",
        "`Both the content as well as the structure are WIP.`\n",
        "\n",
        "ABM is \"particularly well suited to exploring how the aggregate characteristics of some\n",
        "system arise from the behaviour of its parts.\" Lake 2015;\n",
        "ABM is used to understand how the interaction of parts over time dynamically form a whole, i.e., the characteristics of a system, and how that system influences its parts in turn (examples for such systems could be: the evolution of a knowledge domain, the spread and adoption of political thought, economic changes such as industrialization, or changes cultural perceptions). ABM is less about establishing clear-cut, one-way causalities (although those may also play an important part) but more about uncovering complex webs of mutual influences, sometimes called 'feedbacks' or 'feedback loops'.\n",
        "\n",
        "We want to give you a couple of examples of what historically motivated questions with possible ABM application might look like:\n",
        "  - How could changing climate conditions during the small ice age have influenced the interactions of brandenburgian peasants with their environment? Where they able to continue previous practices of extraction? Did they change them, and if yes, how, under which individual, economic, social, and environmental conditions? What change in these interactions could have led to the abandonment or continued survival of villages?\n",
        "  - How did the changes in the built environment after Haussmann's 'renovation of Paris' influence the mobility of Parisians of different quarters? How do the effects on individual mobility compare to similar urban restructuring in London, Berlin, or other cities? How might different groups of people have adapted? Where subsequent urban building decisions more a reaction to the resulting mobility deficits / demands of citizens, or rather driven by policy visions (e.g., car-friendly cities in later decades)\n",
        "  - What structural changes and individual decisions by farmers have led to the widespread adoption of high-intensive farming (i.e., machinization, motorization, and chemization of farming)? Was one of those more impactful than the other, e.g., were farmers 'forced' into it or did they push the structural changes by pioneering the technology? If the latter, what could be the reasons?\n",
        "\n",
        "As you can see, all of these questions revolve around interactions of systemic changes (like climate, policy, or economy) and individual decisions (choosing how to extract resources from nature, choosing a mobility mode, choosing a farming practice). These kinds of questions are not necessarily typical of todays historical sciences, especially for those which concentrate on the micro-perspective of particular individuals. It won't be possible to make an ABM of, for example, the individual Henry V. or, say, one particular nun at a particular monastery, and try to simulate their decisions. While they do emphasise heterogeneity of individual actors, ABMs do deal in aggregates and abstractions of people. This is because a model of anything must be a simplification of the real thing (or person). The crucial part is to choose all those elements of a thing or a person that are the most relevant in the processes the researcher is interested in. The end result of a historical ABM will always be one of many possible, narrowed down perspectives on history, not a perfectly complex, 1-to-1 reconstruction of historical reality. In that, ABM mirrors the hermeneutical process of interpretation in traditional historical science.\n",
        "\n",
        "For that reason, it would be a misconception to characterize this approach as merely reductionist - because at the same time, as hinted at above, fundamental for ABM (and its epistemological parent, systems science) is also the acknowledgement that any complex system will inherently be irreducible to just attributes of their parts, i.e., be explained just by that. This core concept of 'emergence' is a radically anti-reductionst view on reality a majority of historians probably share at least implicitly.\n",
        "\n",
        "#### General Applications of ABM summarized\n",
        "\n",
        "There different kinds of approaches of how to utilize the modeling and simulation of individual agents...\n",
        "\n",
        "- **testing hypotheses** (There are two ways scholars think about this historical process. Try out in a model which perspective leads to results that are more close to what the sources suggest happened.) -> explication of mental / written models, thereby possibly exposing incongruities, fallacies, gaps, biases\n",
        "- **explore and experiment** (We don't yet have good hypotheses about a certain historical process, either because we lack conclusive sources or on the contrary, we have too much information to conclusively process in our brains alone. We will try to build a model that encompasses all that we surmise to be relevant and start experimenting with this model. Thereby, we might get a better idea of which parts of the system or process might be more or less important, or also which knowledge gaps might be the most impactful for our research. This way, avenues to formulating more robust hypotheses might open up - or we might even get a completely new perspective on the process, which opens up new research avenues.\n",
        "- **projection** not (really) applicable for historical research; ABM can be used to try to predict in which directions a system could develop to under certain conditions.\n"
      ],
      "metadata": {
        "id": "lU90EcT5YuVE"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### 1.4 Why do ABM in history?\n",
        "`THIS IS PLACEHOLDER TEXT`\n",
        "\n",
        "`Both the content as well as the structure are WIP.`\n",
        "\n",
        "The general description of ABM might have given you an idea already why this method might be of interest for historians. The following are some of the main reasons why it is well suited for historical research in particular.\n",
        "\n",
        "**1) Modeling (Complex) Dynamics and Processes of History**\n",
        "    \n",
        "ABM pushes into a gap that other currently discussed digital humanities methods leave open, which are often concerned with static data and not the dynamics that connect different data points. A lot of historical inquiry however is revolving around dynamical, complex situations that change over time, as well as around corresponding questions of why and how these situations occurred in a specific way. Current DH methods and purely hermeneutic historical research, too, are not well equipped to model or analyze such time-sensitive dynamics of complex historical systems in a stringent and reproducible way.\n",
        "\n",
        "**2) Modeling heterogenous historical agents**\n",
        "    \n",
        "ABM allows a huge amount of variability and freedom in modeling the behavior and interactions of agents. This makes ABM appealing for historians, who traditionally put a lot of emphasis on complex and creative behavior of human agents. That focus sets ABM apart from solely mathematical simulation methods which are usually less or even *un*able to model heterogeneous individual behavior, as explained briefly above.\n",
        "    \n",
        "**3) Theory-agnostic modeling of historical behaviors**\n",
        "    \n",
        "ABM makes it possible to model assumptions and theories about past behavior of diverse sets of agents with a diverse set of possible interactions. Contrary to a common conception, these assumptions and theories are not limited to a rational-choice paradigm at all (although there exists a large body of research using it for ABM). Of course, the formal modeling of behavior sets technical boundaries to the types of theories that can be represented, so the actual degree of freedom to use a specfic theory needs to be determined in practice.\n",
        "\n",
        "**4) Extensive body of research in adjacent fields**\n",
        "    \n",
        "Not only might this method give a unique perspective to history, it is also tried and tested in many research fields adjacent to history, such as Archaeology, Sociology, Philosophy, or Economics. Especially the first field, Archaeology, has a lot to offer in terms of methodological and epistemological discussions, lessons learned, and specific application questions. A good place to get familiar with some discussions of ABM in Archaeology that are of interest for historians might be {cite}`wurzer_agent-based_2015`.\n",
        "\n",
        "`Until here, we are at approx. 2500 words incl. code\n",
        "`"
      ],
      "metadata": {
        "id": "h7qVekm6Cum-"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### 1.5 Key concepts of ABMs\n",
        "\n",
        "I would write an overview of these points\n",
        "\n",
        "- Agents,\n",
        "- model\n",
        "- space(es)\n",
        "- time\n",
        "- data collection\n",
        "- running many iterations with differerent parameters\n",
        "\n",
        "this points again can be moved up or are already mentioned.\n",
        "\n",
        "- complexity?\n",
        "- modeling in ABM = modeling + formalization + operationalization\n",
        "\n",
        "\n",
        "# 1.6: Simlarities and Differences compared to traditional hermeneutic history\n",
        "`this prob should be integrated in 1.3 why abm in history`\n",
        "- DIFF: strict formalization of concepts / people / behavior!\n",
        "- SIM: iterative, interpretative modeling -> \"hermeneutic figure-8\" (Gavin 2015)\n",
        "- SIM/DIFF: counter-factuals (ie., mental hypothesizing), but explicated and testable\n",
        "- DIFF: experiments!\n",
        "- ...\n"
      ],
      "metadata": {
        "id": "afIeodcKEsMW"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# Part 2: Programming ABMs with Mesa"
      ],
      "metadata": {
        "id": "ZfDz4LFIItLd"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### 2.1 Overview of Mesa\n",
        "\n",
        "- what is mesa?\n",
        "- why mesa and not something else (i.e., what else is there?)?\n",
        "- key concepts of mesa\n",
        "- Link back to top where technical setup is.... or bringing it down here?\n",
        "\n",
        "In this tutorial we will make use of `mesa`, an open-source ABM framework written in Python that has predefined structures to implement the key ingredients of an ABM and supports different space concepts. The package is developed since 2015 and has aquired a large community of users and contributors. Its relative longevity and popularity makes it a good choice to start using ABM with Python.\n",
        "\n",
        "If you are more familiar with other programming languages, you can consider applying the ideas of this tutorial e.g. in the frameworks [NetLogo (dedicated ABM language)](https://ccl.northwestern.edu/netlogo/) or [MASON (Java)](https://cs.gmu.edu/~eclab/projects/mason/).\n",
        "\n",
        "In `mesa` a minimal ABM implementation consists of a definition of the agent class and a model class. The model class holds the model-level attributes, manages the agents, and generally handles the global level of our model. Each instantiation of the model class will be a specific model run. Each model will contain multiple agents, all of which are instantiations of the agent class. Both the model and agent classes are child classes of Mesa’s generic model and agent classes. In line with the above introduced idea of *individual-based* modeling, each agent should have a unique id to allow tracking during the simulation.\n",
        "\n",
        "An important aspect of ABM is the scheduler, that keeps track of when which agent should be activated. In the model class common choices for the scheduler are random, simultaneous, or staged activation, all of which are implemented in mesa. For this tutorial we will make use of random activation.\n",
        "\n",
        "Some research questions might require the agents to interact on a space. This could be a geographical or more abstract space. Since often a notion of closeness is sufficient, the most often implemented type of space is that of a *grid*, either allowing single or more agents in each grid cell. Mesa also supports hexagonal, continues or network grids, which are useful for e.g. covering a geographical space or simulating social relations. IF a simulation relies on geographical map projections, an additional package from the mesa project might be useful: [mesa-geo](https://github.com/projectmesa/mesa-geo).\n"
      ],
      "metadata": {
        "id": "Wvf7sy-oJZPS"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### 2.2 Case Study - An ABM of the Republic of Letters\n",
        "\n",
        "Case Study Goals, Data, general Information\n",
        "\n",
        "- Republic of Letters, where, when?\n",
        "-## (2.2.2?)what kind of data, where does it come from?\n",
        "- whats the research goal?\n",
        "- why abm for this?\n",
        "- what other goals could be achieved with this data and ABM?\n",
        "- what could NOT be done with ABM and this data?\n",
        "- (other possible applications of ABM for typical historical data beyond this case study)\n",
        "- Outline of what will be modeled in this tutorial, and what is left out"
      ],
      "metadata": {
        "id": "znPiEJe2Iw5O"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "### 2.3 Coding\n",
        "\n",
        "- here either copy (and revise) the ABM-workshop jupyter, or come up with something more fitting?\n",
        "  - f.e., could include some more aspects into the lesson that historians will like / find useful... such as: heterogenous agents, validating with historical data/sources, more environment?!, more stuff relating to the historical context...\n",
        "  "
      ],
      "metadata": {
        "id": "o36B1tyiJxOC"
      }
    },
    {
      "cell_type": "markdown",
      "source": [
        "# Part 3: Conclusion\n",
        "\n",
        "–\tGive hints and resources for readers to play around with and extend the model\n",
        "–\tGive reading and coding resources for getting more in-depth into ABM\n",
        "–\tReiterate how this type of model could be a starting place for own research;\n",
        "–\tReiterate peculiarities of ABM for historical research\n",
        "–\tShare data-set?!\n",
        "–\tInvite to get in touch :)\n"
      ],
      "metadata": {
        "id": "wYtrAveERZ-m"
      }
    },
    {
      "cell_type": "code",
      "source": [],
      "metadata": {
        "id": "j-UIZ8pwj8xh"
      },
      "execution_count": null,
      "outputs": []
    }
  ]
}