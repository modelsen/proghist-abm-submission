# ProgHist ABM Submission
This is a repository for the submisison, draft and additional material for a lesson on Agent-based Modeling proposed to the programminghistorian.org website.

- `Lesson.Query.Format.txt` contains the filled out submission template. As of yet, many sections are still bullet points and no proper text.
- `ProgHist-ABM_Lesson_Draft.ipynb` is the draft of the lesson in jupyter notebook format. It's an early version of the draft, with mostly the structure and introductory texts being there, while the code is still missing and many texts are still placeholders or haven't been copy edited yet.
- `mesa_running.webm` is a short clip of part of a running ABM from our project, which would be used to illustrate some functionalities eventually (although they are not yet explained in the lesson draft)
`
